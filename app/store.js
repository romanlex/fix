import {createStore, applyMiddleware, compose } from 'redux';

import reducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk';

const baseHistory = browserHistory;
const routingMiddleware = routerMiddleware(baseHistory);

const enhancerWithDevTools = composeWithDevTools(
	applyMiddleware(thunk, routingMiddleware)
);

export const store = createStore(reducer, enhancerWithDevTools);
export const history = syncHistoryWithStore(baseHistory, store);

