// jQuery
window.$ = window.jQuery = window.jquery = require('../bower_components/jquery/dist/jquery.js');
// Localization
import '../bower_components/jquery-localize-i18n/dist/jquery.localize.js';
// js Storages
window.Storages = require('../bower_components/js-storage/js.storage.js');

// Font Awesome
import '../bower_components/fontawesome/css/font-awesome.min.css';
// Simple line icons
import '../bower_components/simple-line-icons/css/simple-line-icons.css';

// Animate.CSS
import '../bower_components/animate.css/animate.min.css';

// Sweet Alert (global access for other scripts)
import '../bower_components/sweetalert/dist/sweetalert.css';
window.swal = require('../bower_components/sweetalert/lib/sweetalert.js');

// Passfield
import '../bower_components/passfield/dist/js/passfield.min.js';

// Validation
import '../bower_components/jquery-validation/dist/jquery.validate.js';
