import { UserAuthWrapper } from 'redux-auth-wrapper';
import { routerActions } from 'react-router-redux';
import { replace } from 'react-router-redux';
import { addNotification } from '../../actions/notifications';

const _ = require('lodash');

export const UserIsAuthenticated = UserAuthWrapper({
	authSelector: state => state.user.data,
	authenticatingSelector: state => state.user.isFetching,
	//redirectAction: routerActions.replace,
	redirectAction: (newLoc) => (dispatch) => {
		dispatch(replace(newLoc));
		dispatch(addNotification("Для просмотра данной страницы необходимо авторизоваться"));
	},
	wrapperDisplayName: 'UserIsAuthenticated'
});

export const LoginWrapper = UserAuthWrapper({
	authSelector: state => state.user.data,
	redirectAction: routerActions.replace,
	wrapperDisplayName: 'LoginWrapper',
	predicate: _.isEmpty, // user === null || user === {}
	failureRedirectPath: (state, ownProps) => ownProps.location.query.redirect || '/',
	allowRedirectBack: false,
});

export const UserIsAdmin = UserAuthWrapper({
	authSelector: state => state.user.data,
	redirectAction: routerActions.replace,
	failureRedirectPath: '/',
	wrapperDisplayName: 'UserIsAdmin',
	predicate: user => user.isAdmin,
	allowRedirectBack: false
});

export const Authenticated = UserIsAuthenticated((props) => props.children);

