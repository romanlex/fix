// TRANSLATION
// -----------------------------------

const preferredLang = 'ru';
const pathPrefix    = 'dist/server/i18n'; // folder of json files
const packName      = 'site';
const storageKey    = 'fix-application-language';

export default () => {

    if (!$.fn.localize) return;

    // detect saved language or use default
    var currLang = Storages.localStorage.get(storageKey) || preferredLang;
    // set initial options
    var opts = {
        language: currLang,
        pathPrefix: pathPrefix,
        callback: function(data, defaultCallback) {
            Storages.localStorage.set(storageKey, currLang); // save the language
            defaultCallback(data);
        }
    };

    // Set initial language
    setLanguage(opts);

    // Listen for changes
    $(document).on('click', '[data-set-lang]', function() {
        currLang = $(this).data('setLang');
        if (currLang) {
            opts.language = currLang;
            setLanguage(opts);
            activateDropdown($(this));
        }
    });


    function setLanguage(options) {
        $("[data-localize]").localize(packName, options);
    }

    // Set the current clicked text as the active dropdown text
    function activateDropdown(elem) {
        let menu = elem.parents('.language-switcher');
        if (menu.length) {
            elem.siblings("a").removeClass("active");
            elem.addClass("active");
        }
    }

}