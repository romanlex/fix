import React from 'react';
import cx from 'classnames';

class AjaxLoader extends React.Component {

	static propTypes = {
		show: React.PropTypes.bool.isRequired,
	};

	constructor(props) {
		super(props);
	}
	render() {
		let classes = cx({
			'ajax-local-wrapper': true,
			'show': this.props.show
		});
		return (
			<div className={classes}>
				<div className="ajax-local-loader">
					<div className="cssload-fond">
						<div className="cssload-container-general">
							<div className="cssload-internal"><div className="cssload-ballcolor cssload-ball_1"> </div></div>
							<div className="cssload-internal"><div className="cssload-ballcolor cssload-ball_2"> </div></div>
							<div className="cssload-internal"><div className="cssload-ballcolor cssload-ball_3"> </div></div>
							<div className="cssload-internal"><div className="cssload-ballcolor cssload-ball_4"> </div></div>
						</div>
					</div>
				</div>
			</div>

		);
	}

}

export default AjaxLoader;