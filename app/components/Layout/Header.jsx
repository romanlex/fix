import React from 'react';
import { NavDropdown, MenuItem, Row, Col, Grid, Nav} from 'react-bootstrap';
import { Router, Route, Link, History } from 'react-router';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { routerActions } from 'react-router-redux';

import * as Actions from '../../actions/user';

class Header extends React.Component {
    componentDidMount() {

    }
    handleSelect(value) {
        switch (value) {
            case 'logout':
                this.props.actions.logout();
                break;
        }
    }
    render() {
        const { user } = this.props;
        const ddProfileTitle = (
            <span>
                <span className="profile-trigger">
                    <span className="profile-trigger__name">
                        {user.data.name || ''}
                    </span>
                </span>
                <i className="fa fa-angle-down" aria-hidden="true"/>
            </span>
        );
        return (
            <header className="topnavbar-wrapper" id="header">
                <Grid>
                    <Row>
                        <Col lg={16}>
                            <nav role="navigation" className="navbar topnavbar">
                                <div className="navbar-header">
                                    <Link to="/">
                                        <span className="logo">
                                            <em className="icon-settings"/> FIX Summator
                                        </span>
                                    </Link>
                                </div>

                                <div className="nav-wrapper">
                                    <ul className="nav navbar-nav navbar-right">
                                        <NavDropdown noCaret eventKey={ 1 } onSelect={e => this.handleSelect(e)} title={ ddProfileTitle } id="basic-nav-dropdown" className="dropdown--profile">
                                            <MenuItem className="animated flipInX" eventKey={'logout'}>Выйти</MenuItem>
                                        </NavDropdown>
                                    </ul>
                                </div>
                            </nav>
                        </Col>
                    </Row>
                </Grid>
            </header>
            );
    }

}

class MenuItemNotALink extends React.Component {
    render() {
        return (
            <li>
                {this.props.children}
            </li>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
	const { user } = state;
	return {
		user
	}
};

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(Actions, dispatch),
	replace: routerActions.replace
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
