import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { routerActions } from 'react-router-redux';
import * as Actions from '../../actions/notifications';
import cx from 'classnames';


class BasePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          fullHeight: false
        };
        this.setFullHeight = this.setFullHeight.bind(this);
    }

    setFullHeight() {
        this.setState({
          fullHeight: true
        });
    }

    render() {
        let wrapperClass = cx({
			'wrapper': true,
			'fullheight': this.state.fullHeight
		});

        return (
            <div className={wrapperClass}>
                {this.props.children}
            </div>
        );
    }
}

export default BasePage;
