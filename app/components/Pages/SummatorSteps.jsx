import React from 'react';
import { Grid, Row, Col, Panel, Button, FormControl } from 'react-bootstrap';

import Steps from "../Summator/Steps";

import { connect } from 'react-redux';

import cx from 'classnames';

var SummatorSteps = React.createClass({
	getInitialState: function() {
		return {
			currentStep: 1,
			clear: false,
		}
	},

	backStep: function() {
		if(this.state.currentStep <= 1)
			return;

		if(this.state.currentStep === 4)
		{
			this.setState({
				currentStep: 1,
				clear: true,
			});
			return;
		}

		this.setState({
			currentStep: this.state.currentStep - 1,
		});
	},

	nextStep: function() {

		if(this.state.currentStep > 4)
			return;

		this.setState({
			currentStep: this.state.currentStep + 1,
			clear: false
		});


	},

	render: function() {
		const { currentStep, clear } = this.state;
		let backClassess = cx({
			'btn': true,
			'disabled': currentStep === 0
		});
		let nextClassess = cx({
			'btn': true,
			'btn-primary': true,
		});
		return (
			<div className="steps">

				<Steps step={currentStep} clear={clear} goNext={this.nextStep} />

				{
					currentStep !== 3 ?
						<div className="steps__actions">
							<Button onClick={this.backStep} className={backClassess}
									disabled={currentStep <= 1}>
								{currentStep === 4 ? `Вернутся к вводу данных`: `Назад`}
							</Button>
							{
								currentStep !== 4 ?
									<Button onClick={this.nextStep} className={nextClassess}>
										Далее
									</Button>
									: ''
							}
						</div>
						: ''
				}

			</div>
		);
	}

});

export default SummatorSteps;