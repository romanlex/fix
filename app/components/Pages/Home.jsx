import React, { PropTypes } from 'react';
import { Grid, Row, Col, Panel, Button, FormControl } from 'react-bootstrap';
import { Link } from 'react-router';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { routerActions } from 'react-router-redux';

import * as Actions from '../../actions/user';

class Home extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        document.title = "FIX Summator";
    }

    logoutHandler(e) {
        this.props.actions.logout();
    }

    render() {
        const { user } = this.props;
        console.log(user);

        return (
            <div className="home">
                <Grid>
                    <Row>
                        <Col sm={16}>
                            <h1>Описание проекта</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At aut consequuntur cum excepturi id impedit, in, ipsum, maxime molestias mollitia nobis nulla obcaecati perferendis perspiciatis quisquam quo sunt velit veritatis?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At aut consequuntur cum excepturi id impedit, in, ipsum, maxime molestias mollitia nobis nulla obcaecati perferendis perspiciatis quisquam quo sunt velit veritatis?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At aut consequuntur cum excepturi id impedit, in, ipsum, maxime molestias mollitia nobis nulla obcaecati perferendis perspiciatis quisquam quo sunt velit veritatis?</p>
                            {
                                !user.isAuthenticated ?
                                    <Link to="/login" className="btn btn-primary">
                                        Войти
                                    </Link>
                                :
                                    <div>
                                        <Button className="btn btn-danger" onClick={this.logoutHandler.bind(this)}>
                                            Выйти
                                        </Button>
                                        <Link to="/summator" className="btn btn-default">
                                            Открыть приложение
                                        </Link>
                                    </div>


                            }
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}

const mapStateToProps = (state, ownProps) => {
	const { user } = state;
	return {
		user
	}
};

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(Actions, dispatch),
	replace: routerActions.replace
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
