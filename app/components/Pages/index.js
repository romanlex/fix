import Login from './Login';
import NotFound from './NotFound';
import Summator from './Summator';
import Home from './Home';

module.exports = { Login, NotFound, Summator, Home };