import React from 'react';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import { Router, Route, Link, History } from 'react-router';

class NotFound extends React.Component {

    render() {
        return (
            <div className="abs-center fullscreen flex h-m v-m">
                <div className="text-center mb-xl">
                    <div className="text-lg mb-lg">404</div>
                    <p className="lead m0">Страница не найдена.</p>
                    <p>Страница которую вы запросили не существует.</p>
                </div>
                <ul className="list-inline text-center text-sm mb-xl">
                    <li><Link to="/" className="text-muted">Назад на главную</Link></li>
                    <li className="text-muted">|</li>
                    <li><Link to="/login" className="text-muted">Войти</Link></li>
                </ul>
            </div>
            );
    }

}

export default NotFound;

