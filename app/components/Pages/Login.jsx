import React, { PropTypes } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { routerActions } from 'react-router-redux';

import * as Actions from '../../actions/user';
import Notify from '../Common/notify';
import AjaxLoader from "../Elements/AjaxLoader";
import cx from 'classnames';
import { Link } from 'react-router';

export class Login extends React.Component {
    static propTypes = {
        replace: PropTypes.func.isRequired
    };

    constructor(props) {
		super(props);
		this.state = {
			login: '',
			password: '',
			remember: true,
			isFetching: false
		};
	}

	componentWillMount() {
		const { isAuthenticated, replace, redirect } = this.props;
		console.log('isAuthenticated', isAuthenticated);
		if (isAuthenticated) {
			replace(redirect)
		}
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.user.isFetching)
			return;

		console.log(nextProps);
		const { isAuthenticated, replace, redirect } = nextProps;
		const { isAuthenticated: wasAuthenticated }  = this.props;

		if (!wasAuthenticated && isAuthenticated) {
			replace(redirect);
		} else {
			this.setState({
				login: '',
				password: ''
			});
			Notify("Не верно указано имя пользователя или пароль.", {status: 'danger'});
		}
	}

    componentDidMount() {
        document.title = "Авторизация";
		const run = () => {
			$('#credentials').passField({
				showToggle: true,
				showGenerate: false,
				showWarn: false,
				showTip: false,
                maskBtn : {
                    textMasked : "",
                    textUnmasked: "",
                    className: true,
                    classMasked: 'masked',
                    classUnmasked: 'unmasked'
                }
			});
		};
		run();
	}

	handleLogin(e) {
		e.preventDefault();
		this.props.actions.login(this.state);
	}

	handleInputChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState({
		  [name]: value
		});
	}

	insertAttr(e) {
    	e.preventDefault();
    	this.setState({
			login: 'guest@fix.ru',
			password: 'test'
		})
	}

    render() {
    	const { user } = this.props;
    	let isValid = false;
		if(this.state.password && validateEmail(this.state.login))
			isValid = true;

    	const buttonClasses = cx({
			'pull-left': true,
			'btn': true,
			'btn-primary': isValid,
			'disabled': !isValid
		});

        return (
            <div className="block-center fullscreen flex h-m v-m">
                <div className="panel panel--login">
					<AjaxLoader show={user.isFetching} />
                    <div className="panel-heading text-center">
						<Link to="/">
							<span className="logo">
								<em className="icon-settings"/> FIX Summator
							</span>
						</Link>
                    </div>
                    <div className="panel-body">
                        <div className="panel-body__content">
                            <p className="text-center panel-title">Войти</p>
                            <form role="form" action="#" method="post" onSubmit={e => this.handleLogin(e)}>
                                <div className="form-group">
                                    <input id="login" type="email" ref="login" name="login" placeholder="E-mail" autoComplete="off" required="required" className="form-control" value={this.state.login} onChange={e => this.handleInputChange(e)} />
                                </div>
                                <div className="form-group nomargin">
                                    <input id="credentials" type="password" ref="password" name="password" placeholder="Пароль" required="required" className="form-control" value={this.state.password} onChange={e => this.handleInputChange(e)} />
                                </div>
                                <div className="clearfix panel-buttons">
                                    <button type="submit" className={buttonClasses} disabled={!isValid}>Войти</button>
                                </div>
                            </form>
							<span className="help">
								guest@fix.ru:test <a href="#" onClick={e => this.insertAttr(e)}>[insert credentials]</a>
							</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

const mapStateToProps = (state, ownProps) => {
	const { user } = state;
	const isAuthenticated = user.data.isAuthenticated || false;
	const redirect = ownProps.location.query.redirect || '/';
	return {
		isAuthenticated,
		user,
		redirect
	}
};

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(Actions, dispatch),
	replace: routerActions.replace
});


//export default connect(mapStateToProps, mapDispatchToProps)(ProjectInterviews);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
