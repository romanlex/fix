import React from 'react';
import { Grid, Row, Col, Panel, Button, FormControl } from 'react-bootstrap';
import SummatorSteps from "./SummatorSteps";

export default function Summator({ authData }) {
	const login = filterEmail(authData.login);
	return (
		<div className="summator">
			<Row>
				<Col sm={8} className="summator__left">
					<h1>Привет, {login || `login`}!</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto atque beatae dignissimos excepturi fugit inventore odit ut. Aliquid dolore id, iusto laboriosam, modi nulla perferendis rem, saepe sunt veniam vitae!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto atque beatae dignissimos excepturi fugit inventore odit ut. Aliquid dolore id, iusto laboriosam, modi nulla perferendis rem, saepe sunt veniam vitae!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto atque beatae dignissimos excepturi fugit inventore odit ut. Aliquid dolore id, iusto laboriosam, modi nulla perferendis rem, saepe sunt veniam vitae!</p>
				</Col>
				<Col sm={8} className="summator__right">
					<SummatorSteps />
				</Col>
			</Row>
		</div>
	)
}

function filterEmail(email) {
	let box = email.split('@')[0];
	let domain = getDomainFromEmail(email);
	domain = domain.split('.');
	let firstLetterDomain = domain[0][0];
	let filterString = '';
	for(let i = 0; i < domain[0].length - 1; i++) {
		filterString += '*';
	}
	return `${box}@${firstLetterDomain}${filterString}.${domain[1]}`;

}

function getDomainFromEmail(email) {
	let domain = email.replace(/.*@/, "");
	return domain;
}