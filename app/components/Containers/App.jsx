import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { routerActions } from 'react-router-redux';
import * as Actions from '../../actions/notifications';
import Notify from '../Common/notify';

class App extends Component {

	constructor(props) {
		super(props);
	}

	componentDidMount() {

	}

	componentWillReceiveProps() {

	}

	render() {
		const { notifications } = this.props;
		if(notifications.messages.length > 0) {
            notifications.messages.map((value, index) => {
                Notify(value || "", {status: 'danger'});
                this.props.actions.removeNotification(index);
            })
        }

		return 	React.cloneElement(this.props.children, {
					key: this.props.location.pathname
				});
	}
}


const mapStateToProps = (state, ownProps) => {
	const { notifications } = state;
	return {
		notifications,
	}
};

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(Actions, dispatch),
	replace: routerActions.replace
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

