import React from 'react';

import Step1 from "../Summator/Step1";
import Step2 from "../Summator/Step2";
import Step3 from "../Summator/Step3";
import Step4 from "../Summator/Step4";

const _ = require('lodash');
import Notify from '../Common/notify';

const Components = {
    1: Step1,
    2: Step2,
    3: Step3,
    4: Step4,
};

const initialState = {
	numbers: [
		{
			key: 1,
			name: 'number1',
			value: '',
			title: 'Число 1'
		},
		{
			key: 2,
			name: 'number2',
			value: '',
			title: 'Число 2'
		},
	],
	result: 0
};

var Steps = React.createClass({
	getInitialState: function() {
		return initialState;
	},

	clearData: function() {
		this.setState({
			numbers: [
				{
					key: 1,
					name: 'number1',
					value: '',
					title: 'Число 1'
				},
				{
					key: 2,
					name: 'number2',
					value: '',
					title: 'Число 2'
				},
			],
			result: 0
		});
	},

	addNumber: function(event) {
		let numbers = this.state.numbers;
		let newKey = 1;

		if(numbers.length > 1) {
			let lastKey = numbers[numbers.length - 1].key;
			newKey  = lastKey + 1;
		}

		let newObject = {
			key: newKey,
			name: `number${newKey}`,
			value: '',
			title: `Число ${newKey}`
		};
		numbers.push(newObject);
		this.setState({
			numbers: numbers
		});
	},

	handleInputChange: function(event) {
		const target = event.target;
		let value;
		switch (target.type) {
			case 'checkbox':
				value = target.checked;
				break;
			case 'number':
				value = parseInt(target.value);
				break;
			default:
				value = target.value;
		}

		if(value.length > 0 && !/^\d+$/.test(value))
		{
			Notify('Необходимо ввести числовое значение', {status: 'danger'});
			return;
		}
		const name = target.name;
		let numbers = this.state.numbers;
		let index = _.findIndex(numbers, function (o) {
			return o.name === name;
		});
		numbers[index].value = value;

		this.setState({
			numbers: numbers
		});
	},

	setResult: function (result) {
		this.props.goNext();
		this.setState({
			result: result
		});

	},

	render: function() {
		const { step } = this.props;
		const Component = Components[step];
		return  (
				<div key="step">
					<Component numbers={this.state.numbers}
							   needClear={this.props.clear}
							   clear={this.clearData}
							   result={this.state.result}
							   addNumber={this.addNumber}
							   handleInputChange={this.handleInputChange}
							   setResult={this.setResult} />
				</div>
		);
	}

});

export default Steps;