import React from 'react';

class Step2 extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		const {numbers} = this.props;

		return (
			<div className="step--two">
				<h4 className="step__title">
					Шаг #2 - Подтверждение данных
				</h4>
				<div className="step__content">
					Проверьте корректность ваших данных:<br />
					<table className="table">
						<thead>
							<tr>
								<td>Имя</td>
								<td>Значение</td>
							</tr>
						</thead>
						<tbody>
						{
							numbers.map((input, index) => {
								return (
									<tr key={index}>
										<td>{input.title}</td>
										<td>{input.value || `не участвует в расчете`}</td>
									</tr>
								);
							})
						}
						</tbody>
					</table>
				</div>
			</div>
		);
	}

}

export default Step2;