import React from 'react';
import { NavDropdown, MenuItem, Row, Col, Button, Grid, Panel, Nav, NavItem, Dropdown, FormGroup, FormControl } from 'react-bootstrap';


class Step1 extends React.Component {

	constructor(props) {
		super(props);
	}
	changeInput(e) {
		this.props.handleInputChange(e);
	}

	clickAddButton(e) {
		this.props.addNumber();
	}

	componentDidMount() {
		console.log(this.props);
		if(this.props.needClear)
		{
			this.props.clear();
		}
	}

	render() {
		const { numbers } = this.props;
		return (
			<div className="step--one">
				<h4 className="step__title">
					Шаг #1 - Ввод данных
				</h4>
				<div className="step__content">
					<form action="#" className="form-horizontal">
						{
							numbers.map((input, index) => {
								return (<FormGroup key={index}>
									<label className="col-lg-3 col-md-4 control-label">{input.title}</label>
									<Col lg={ 8 } md={8}>
										<FormControl type="text"
													 step="1"
													 name={input.name}
													 required="required"
													 onChange={e => this.changeInput(e)}
													 value={input.value}
													 placeholder=""
													 className="form-control"
													 autoComplete="off"/>
									</Col>
								</FormGroup>);
							})
						}

						<div className="clearfix"/>
					</form>
					<Button className="btn btn-xs" onClick={e => this.clickAddButton(e)}>
						Добавить число
					</Button>
				</div>
			</div>
		);
	}

}

export default Step1;