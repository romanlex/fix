import React from 'react';

class Step4 extends React.Component {

	render() {
		const { numbers, result } = this.props;
		return (
			<div className="step--four">
				<h4 className="step__title">
					Шаг #4 - Результат
				</h4>
				<div className="step__content">
					Ваши данные:<br />
					<table className="table">
						<thead>
							<tr>
								<td>Имя</td>
								<td>Значение</td>
							</tr>
						</thead>
						<tbody>
						{
							numbers.map((input, index) => {
								return (
									<tr key={index}>
										<td>{input.title}</td>
										<td>
											{
												input.value > 10 ?
													<span className="label label-success">{input.value}</span>
													: input.value || `не участвует в расчете`
											}
										</td>
									</tr>
								);
							})
						}
						</tbody>
					</table>
					Результаты расчета: <span className="label label-primary">{result}</span>
				</div>
			</div>
		);
	}

}

export default Step4;