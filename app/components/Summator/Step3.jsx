import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { routerActions } from 'react-router-redux';
import * as Actions from '../../actions/calculation';
import AjaxLoader from "../Elements/AjaxLoader";

class Step3 extends React.Component {

	componentDidMount() {
		this.props.actions.getSum(this.props.numbers);
	}

	componentWillReceiveProps(nextProps) {
		const { calculation } = nextProps;
		if(calculation.result > 0){
			this.props.setResult(calculation.result);
		}
	}

	render() {
		const { calculation } = this.props;
		// if(!calculation.isFetching)
		// 	this.props.goNext();

		return (
			<div className="step--three">
				<h4 className="step__title">
					Шаг #3 - Расчет
				</h4>
				<div className="step__content">
					<AjaxLoader show={calculation.isFetching} />
				</div>
			</div>
		);
	}

}

const mapStateToProps = (state, ownProps) => {
	const {
		calculation: calculation
	} = state.calculation;
	return {
		calculation
	}
};

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(Actions, dispatch),
	replace: routerActions.replace
});


export default connect(mapStateToProps, mapDispatchToProps)(Step3)