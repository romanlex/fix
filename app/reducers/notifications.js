import {
	NOTIFICATION_ADDED,
	NOTIFICATION_REMOVE
} from '../actions/notifications'


export default function notifications(state = {
		messages: []
	}, action) {
	switch (action.type) {
		case NOTIFICATION_ADDED:
			let messages = state.messages;
			messages.push(action.payload);
			return {...state, messages: messages};
		case NOTIFICATION_REMOVE:
			const messagesFiltered = state.messages;
			delete messagesFiltered[action.payload];
			console.log(messagesFiltered);
			return { state, messages: messagesFiltered };
		default:
			return state;
	}
}