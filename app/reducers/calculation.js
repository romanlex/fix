import {
	SUM,
	SUM_START
} from '../actions/calculation'


export default function calculation(state = {
		calculation: {
			isFetching: true,
			result: 0
		}
	}, action) {
	switch (action.type) {
		case SUM_START:
			return {...state, calculation: {
				isFetching: true
			}};
		case SUM:
			return {...state, calculation: {
				isFetching: false,
				result: action.payload
			}};
		default:
			return state;
	}
}