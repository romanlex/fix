import {
	USER_LOGGED_IN,
	USER_LOGGED_OUT,
	USER_LOGGED_IN_START,
} from '../actions/user'


export default function user(state = {
		isFetching: false,
		isAuthenticated: (!!Storages.localStorage.get('token')),
		data: Storages.localStorage.get('token') || {}
	}, action) {
	switch (action.type) {
		case USER_LOGGED_IN_START:
			console.log('USER_LOGGED_IN_START');
			return {...state, isFetching: true};
		case USER_LOGGED_IN:
			console.log('USER_LOGGED_IN');
			return {...state, data: action.payload, isFetching: false, isAuthenticated: action.isAuthenticated};
		case USER_LOGGED_OUT:
			return {...state, data: {}, isFetching: false, isAuthenticated: false };
		default:
			return state;
	}
}