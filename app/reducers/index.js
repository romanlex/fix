import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import user from './user';
import notifications from './notifications';
import calculation from './calculation';

export default combineReducers({
    user,
    calculation,
    notifications,
    routing: routerReducer,
})
