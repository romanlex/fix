export const NOTIFICATION_ADDED  = 'NOTIFICATION_ADDED';
export const NOTIFICATION_REMOVE  = 'NOTIFICATION_REMOVE';

export const addNotification = (message) => dispatch => {
	dispatch({
		type: NOTIFICATION_ADDED,
		payload: message
	});
};

export const removeNotification = (index) => dispatch => {
	dispatch({
		type: NOTIFICATION_REMOVE,
		payload: index
	})
};




