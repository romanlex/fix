export const SUM  = 'SUM';
export const SUM_START  = 'SUM_START';

function fetchingStart() {
	return {
		type: SUM_START,
	};
}

export const getSum = (values) => dispatch => {
	dispatch(fetchingStart());
	let result = 0;
	Object.keys(values).map(function(objectKey, index) {
		let value = values[objectKey];
		result += parseInt(value.value);
	});
	setTimeout(() => {
		dispatch({
			type: SUM,
			payload: result
		});
	},1500);
};



