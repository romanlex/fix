export const USER_LOGGED_IN  = 'USER_LOGGED_IN';
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT';
export const USER_LOGGED_IN_START = 'USER_LOGGED_IN_START';

let user = {
	name: 'Роман',
	surname: 'Олин',
	login: 'guest@fix.ru',
	password: 'test',
};

function loggedInStart() {
	return {
		type: USER_LOGGED_IN_START,
	};
}

export const login = (credentials) => dispatch => {
	dispatch(loggedInStart());
	setTimeout(() => {
		if(credentials.login === user.login && credentials.password === user.password) {
			Storages.localStorage.set('token', JSON.stringify(user));
			dispatch({
				type: USER_LOGGED_IN,
				isAuthenticated: true,
				payload: user
			});
		} else {
			dispatch({
				type: USER_LOGGED_IN,
				isAuthenticated: false,
				payload: {}
			});
		}
	}, 500);
};

export const logout = () => dispatch => {
	Storages.localStorage.remove('token');
	dispatch({
		type: USER_LOGGED_OUT,
	});
};



