import React from 'react';
import ReactDOM from 'react-dom';
import { FastClick } from 'fastclick';
import { AppContainer } from 'react-hot-loader';

import { history, store } from './store';
import { Router, Route, IndexRoute, IndexRedirect, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { UserIsAuthenticated, UserIsNotAuthenticated, LoginWrapper, Authenticated } from './components/Common/wrappers';

import initTranslation from './components/Common/localize';

import BasePage from './components/Layout/BasePage';
import Base from './components/Layout/Base';
import { Login, Home, NotFound, Summator } from './components/Pages/';

/*
* Application Styles
*/
import './styles/bootstrap.scss';
import './styles/app.scss';
import App from "./components/Containers/App";

/*
* Init translation system
*/
initTranslation();

ReactDOM.render(
    <AppContainer>
        <Provider store={store}>
            <Router key={module.hot && new Date()} history={history}>

                <Route path="/" component={App}>
                    <Route component={BasePage}>
                        <IndexRoute component={Home} />
                        <Route path="login" component={LoginWrapper(Login)}/>
                    </Route>

                    <Route component={Base}>
                        <Route path="summator" component={UserIsAuthenticated(Summator)}/>
                    </Route>
                </Route>

                <Route path="*" component={NotFound}/>

            </Router>
        </Provider>
    </AppContainer>,
    document.getElementById('app')
);

// Handle errors that might happen after rendering
// Display the error in full-screen for development mode
// if (__DEV__) {
//     window.addEventListener('error', (event) => {
//         appInstance = null;
//         document.title = `Runtime Error: ${event.error.message}`;
//         ReactDOM.render(<ErrorReporter error={event.error} />, container);
//     });
// }

/*
* Webpack HMR
*/
if (module.hot) {
    module.hot.accept('./components/Containers/App', () => {
        const newApp =  require('./components/Containers/App').default;
        render(newApp);
    });

    module.hot.accept('./reducers', () => {
        store.replaceReducer(require('./reducers').default);
    });
}

/* Attach fastclick fo document body */
FastClick.attach(document.body);