/* eslint-disable */
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import WebpackDevServer from 'webpack-dev-server';
import config from './webpack.config.js';

import clean from './clean';
import copy from './copy';
import run from './run';

const path    = require('path');
const isDebug = !process.argv.includes('--release');

async function start() {
	await run(clean);
	await run(copy);
	await new Promise((resolve) => {

		// if (isDebug) {
		//     config.entry.client = [...new Set([
		//         'babel-polyfill',
		//         'react-hot-loader/patch',
		//         'webpack-hot-middleware/client',
		//     ].concat(config.entry.client))];
		//     config.output.filename = config.output.filename.replace('[chunkhash', '[hash');
		//     config.output.chunkFilename = config.output.chunkFilename.replace('[chunkhash', '[hash');
		//     const { query } = config.module.rules.find(x => x.loader === 'babel-loader');
		//     query.plugins = ['react-hot-loader/babel'].concat(query.plugins || []);
		//     config.plugins.push(new webpack.HotModuleReplacementPlugin());
		//     config.plugins.push(new webpack.NoEmitOnErrorsPlugin());
		// }

		const server = new WebpackDevServer(webpack(config), {
			hot: true,
			inline: true,
			historyApiFallback: true
		});

		// Important part. Send down index.html for all requests
		server.use('/', function (req, res) {
			res.sendFile(path.resolve(__dirname, '../dist/index.html'));
		});

		server.listen(config.devServer.port, 'localhost', function (err, result) {
			if (err) {
				return console.log(err);
			}
			console.log(`Listening at http://localhost:${config.devServer.port}`);
		});
	});
}


export default start;