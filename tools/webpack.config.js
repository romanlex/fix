/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable global-require, no-confusing-arrow, max-len */

const path              = require('path');
const webpack           = require('webpack');
const AssetsPlugin      = require('assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const WriteFilePlugin   = require('write-file-webpack-plugin');
const pkg               = require('../package.json');


const isDebug   = global.DEBUG === false ? false : !process.argv.includes('--release');
const isVerbose = process.argv.includes('--verbose') || process.argv.includes('-v');
const useHMR    = !process.argv.includes('--no-hmr'); // Hot Module Replacement (HMR)
const isAnalyze = process.argv.includes('--analyze') || process.argv.includes('--analyse');

let babelConfig = Object.assign({}, pkg.babel, {
	babelrc: false,
	cacheDirectory: useHMR,
	presets: pkg.babel.presets.map(x => x === 'latest' ? ['latest', { es2015: { modules: false } }] : x),
});

// Webpack configuration (main.js => public/dist/main.{hash}.js)
// http://webpack.github.io/docs/configuration.html
const config = {

	// The base directory for resolving the entry option
	context: path.resolve(__dirname, '../app'),

	// The entry point for the bundle
	entry: [
		'vendor.js',
		'index.js'
	],
	resolve: {
		modules: [path.resolve(__dirname, "../app"), 'bower_components', 'node_modules'],
		extensions: ['.js', '.jsx', '.css', '.scss', 'jpg', 'png', 'svg'],
		descriptionFiles: ["package.json", "bower.json"],
	},
	// Options affecting the output of the compilation
	output: {
		path: path.resolve(__dirname, '../dist/'),
		publicPath: `/`,
		filename: isDebug ? 'app.js?[hash]' : 'app.min.js',
		chunkFilename: isDebug ? '[id].js?[chunkhash]' : '[id].[chunkhash].js',
		sourcePrefix: '  ',
	},

	devServer: {
		port: 3000,
		publicPath: path.resolve(__dirname, '../dist/'),
		contentBase: path.resolve(__dirname, '../dist/'),
		compress: true
	},

	// Developer tool to enhance debugging, source maps
	// http://webpack.github.io/docs/configuration.html#devtool
	devtool: isDebug ? 'source-map' : false,

	// What information should be printed to the console
	stats: {
		colors: true,
		reasons: isDebug,
		hash: isVerbose,
		version: isVerbose,
		timings: true,
		chunks: isVerbose,
		chunkModules: isVerbose,
		cached: isVerbose,
		cachedAssets: isVerbose,
	},

	// The list of plugins for Webpack compiler
	plugins: [
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': isDebug ? '"development"' : '"production"',
			__DEV__: isDebug
		}),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, '../app/index.html'),
			baseUrl: '/'
		}),
		new webpack.LoaderOptionsPlugin({
			debug: isDebug,
			minimize: !isDebug,
		}),
		new ExtractTextPlugin('css/style-[hash].css'),
		// new WriteFilePlugin(),
		// new CopyWebpackPlugin([{
		// 	context: path.resolve(__dirname, '..'),
		// 	from: 'app/img',
		// 	to: 'img',
		// }, {
		// 	context: path.resolve(__dirname, '..'),
		// 	from: 'app/fonts',
		// 	to: 'fonts',
		// }]),
		// new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery",
		})
	],

	// Options affecting the normal modules
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				options: babelConfig
			},
			{
				test: /\.json$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'json-loader',
			},
			{
				test: /\.css$/,
				exclude: path.join(process.cwd(), '../app'),
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader'
				})
			},
			{
				test: /\.css$/,
				include: path.join(process.cwd(), '../app'),
				use: [
					{
						loader: 'style-loader',
					},
					{
						loader: 'css-loader',
						options: {
							sourceMap: isDebug,
							importLoaders: true,
							// CSS Modules https://github.com/css-modules/css-modules
							modules: true,
							localIdentName: isDebug ? '[name]_[local]_[hash:base64:3]' : '[hash:base64:4]',
							// CSS Nano http://cssnano.co/options/
							minimize: !isDebug,
							discardComments: { removeAll: true },
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							config: './tools/postcss.config.js',
						},
					},

				],
			},
			{
				test: /\.scss$/,
				use: [
					{
						loader: 'style-loader',
					},
					{
						loader: 'css-loader',
					},

					{
						loader: 'postcss-loader',
						options: {
							config: './tools/postcss.config.js',
						},
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: isDebug,
							outputStyle: "expanded"
						}
					},
				],
			},
			{
				test: /\.(woff|woff2|eot|otf|ttf)$/,
				exclude: /node_modules/,
				use: 'url-loader?limit=20480&name=fonts/[name].[ext]',
			},
			{
				test: /\.(png|jpg?g|gif|svg)$/,
				exclude: /node_modules/,
				use: 'url-loader?limit=20480&name=img/[name]-[hash].[ext]',
			},
		],
	},
	bail: !isDebug,

	cache: isDebug,

	stats: {
		colors: true,
		reasons: isDebug,
		hash: isVerbose,
		version: isVerbose,
		timings: true,
		chunks: isVerbose,
		chunkModules: isVerbose,
		cached: isVerbose,
		cachedAssets: isVerbose,
	},
};

// Optimize the bundle in release (production) mode
if (!isDebug) {
	config.plugins.push(
		new webpack.optimize.CommonsChunkPlugin({
			children: true,
			asycn: true,
			minChunks: 2,
		}),
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.optimize.UglifyJsPlugin({
			beautify: false,
			comments: false,
			compress: {
				sequences: true,
				booleans: true,
				loops: true,
				warnings: false,
				drop_console: true,
				unsafe: true
			}
		}),
	);
}


// Hot Module Replacement (HMR) + React Hot Reload
if (isDebug && useHMR) {
	babelConfig.plugins.unshift('react-hot-loader/babel');
	config.entry.unshift('react-hot-loader/patch', 'webpack-dev-server/client?http://localhost:3000', 'webpack/hot/only-dev-server');
	config.plugins.push(new webpack.HotModuleReplacementPlugin());
	config.plugins.push(new webpack.NoEmitOnErrorsPlugin());
}


module.exports = config;
